package cl.autentia.myapplication;

// Copyright (c) Edison Saez. All Rights Reserved.

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText etTextoEntrada;
    TextView tCoded, tDec;
    String HEXDIGITS = "0123456789ABCDEF";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etTextoEntrada = findViewById(R.id.etTexto);
        tCoded = findViewById(R.id.coded);
        tDec = findViewById(R.id.dec);

        etTextoEntrada.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String txt = charSequence.toString();
                if (txt.length() < 4){
                    txt = addSpaces(txt);
                }
                String secuenciaEncoded = bytesAsHex(txt.getBytes());
                tCoded.setText("0x"+secuenciaEncoded);
                tDec.setText(String.valueOf(getDecimal(secuenciaEncoded)));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    public String addSpaces(String str){
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < (4-str.length()); i++) {
            builder.append(" ");
        }
        return builder.toString() + str;
    }

    public String bytesAsHex(byte[] bytes) {

        if (bytes == null)
            return "<NULL>";
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(HEXDIGITS.charAt((b & 0xf0) >> 4));
            sb.append(HEXDIGITS.charAt(b & 0x0f));
        }
        return  sb.toString();
    }

    public int getDecimal(String hex){

        hex = hex.toUpperCase();
        int val = 0;
        for (int i = 0; i < hex.length(); i++)
        {
            char c = hex.charAt(i);
            int d = HEXDIGITS.indexOf(c);
            val = 16*val + d;
        }
        return val;
    }
}